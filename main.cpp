
#include <iostream>
#include <conio.h>
#include <ctime>
#include <fstream>
#include <string>
#include <vector>



using namespace std;

void wait(double seconds);


class Gra 
{
private:

	static const int dl_toru = 10;
	static const int sz_toru = 6;
	int tor[dl_toru][sz_toru];
	int wynik;
	int polozenie_autka;

	friend class Menu;

public:

	bool czy_brak_kolizji;

	Gra() 
	{
		for (int i = 0; i < dl_toru; i++) 
		{
			for (int j = 0; j < sz_toru; j++) 
			{
				this->tor[i][j] = 0;
			}
		}
		for (int i = 0; i < dl_toru; i++) {
			this->tor[i][0] = 3;
			this->tor[i][sz_toru - 1] = 3;
		}
		this->tor[dl_toru - 1][2] = 2;
		this->polozenie_autka = 2;
		this->czy_brak_kolizji = true;
		this->wynik = 0;
	}

	void Reset() 
	{
		for (int i = 0; i < dl_toru; i++) 
		{
			for (int j = 0; j < sz_toru; j++) 
			{
				this->tor[i][j] = 0;
			}
		}
		for (int i = 0; i < dl_toru; i++) 
		{
			this->tor[i][0] = 3;
			this->tor[i][sz_toru - 1] = 3;
		}
		this->tor[dl_toru - 1][2] = 2;
		this->polozenie_autka = 2;
		this->czy_brak_kolizji = true;
		this->wynik = 0;
	}

	void ZwiekszWynik() 
	{
		this->wynik++;
	}

	int ZwrocWynik() 
	{
		return this->wynik;
	}


	void PrzesunAutko(char kierunek) 
	{
		if (this->czy_brak_kolizji == true) 
		{
			if (this->polozenie_autka > 1)
			{
				if (kierunek == 'a' || kierunek == 'A') 
				{
					this->tor[dl_toru - 1][polozenie_autka] = 0;
					this->polozenie_autka--;
					this->tor[dl_toru - 1][polozenie_autka] = 2;
				}
			}
			if (this->polozenie_autka < sz_toru - 2) 
			{
				if (kierunek == 'd' || kierunek == 'D') 
				{
					this->tor[dl_toru - 1][polozenie_autka] = 0;
					this->polozenie_autka++;
					this->tor[dl_toru - 1][polozenie_autka] = 2;
				}
			}
		}
	}

	void TworzPrzeszkode() 
	{
		srand(time(NULL));
		int miejsce_wstawiania;
		miejsce_wstawiania = 1 + (rand() % (sz_toru - 2));
		this->tor[0][miejsce_wstawiania] = 1;
	}

	void Przesun() 
	{
		for (int i = dl_toru -1 ; i >=0; i--)
		{
			for (int j = 1; j < sz_toru-1; j++)
			{
				if (this->tor[i][j] == 1)
				{
					if (this->tor[i + 1][j] == 2) 
					{
						this->czy_brak_kolizji = false;
					}
					(*(*(this->tor + i+1) + j)) = 1;
					(*(*(this->tor + i) + j)) = 0;
				}
			}
		}
	}

	void WyswietlTor()
	{
		char znaki[4] = { ' ', 'O', 'H', 'X' };
		for (int i = 0; i < dl_toru; i++) 
		{
			for (int j = 0; j < sz_toru; j++) 
			{
				cout << *(znaki + (*(*(this->tor + i) + j)));
			}
			cout << endl;
		}
	}

};

class Wynik
{
private:
	
	string zawartosc;


public:
	Wynik()
	{
		this->zawartosc = "";
	}

	//wzi�te z neta
	int StrToInt(string s)
	{
		bool m = false;
		int tmp = 0;
		int i = 0;
		if (s[0] == '-')
		{
			i++;
			m = true;
		}
		while (i<s.size())
		{
			tmp = 10 * tmp + s[i] - 48;
			i++;
		}
		return m ? -tmp : tmp;
	}

	string IntToStr(int n)
	{
		string tmp, ret;
		if (n < 0) {
			ret = "-";
			n = -n;
		}
		do {
			tmp += n % 10 + 48;
			n -= n % 10;
		} while (n /= 10);
		for (int i = tmp.size() - 1; i >= 0; i--)
			ret += tmp[i];
		return ret;
	
	}
	// do t�d

	void WyswietlWyniki()
	{
		this->zawartosc = "";
		
		ifstream plik("wyniki.txt");
		
		
			if (plik.is_open())
			{
				cout << "Ostatinie wyniki :\n\nWynik:\tGracz:\t\tData i czas:\n";
				int i = 0;
				while (!plik.eof()&&i<10)
				{
					string wiersz;

					getline(plik, wiersz);
					wiersz += "\n";
					zawartosc += wiersz;
					i++;
				}
				cout << zawartosc << endl << endl;
				this->zawartosc = "";
				plik.close();
			}
			else
			{
				cout << "ERROR: Nie znaleziono pliku wyniki.txt!";
			}
		
	}


	void DodajWynik(int wynik, string gracz) {
		this->zawartosc = "";
		ifstream plik_odczyt("wyniki.txt");
		
		if (plik_odczyt.is_open())
		{
			
			while (!plik_odczyt.eof())
			{
				string wiersz;

				getline(plik_odczyt, wiersz);
				wiersz += "\n";
				this->zawartosc += wiersz;
				
			}
			plik_odczyt.close();

			fstream plik("wyniki.txt", ios::out | ios::trunc);
			plik << IntToStr(wynik) << "\t" << gracz << "\t\t" << __DATE__ << " " << __TIME__ << "\n" << this->zawartosc;
			plik.flush();
			plik.close();
		}
		else
		{
			cout << "ERROR: Nie znaleziono pliku wyniki.txt!";
		}

	}

	void CzyscWyniki() {
		fstream plik("wyniki.txt", ios::in | ios::out | ios::trunc);
		plik.close();
	}

};


class Menu 
{
private:
	string gracz = "Player";

public:
	Wynik score;
	double szybkosc = 0.5;
	bool graON;
	bool start;

	Menu() 
	{
		this->graON = true;
		this->start = false;
	}
	void SplashScreen()
	{

		string tekst = "";
		ifstream Splash("splash.txt");

		if (Splash.is_open())
		{
			while (!Splash.eof())
			{
				string wiersz;

				getline(Splash, wiersz);
				wiersz += "\n";
				tekst += wiersz;
			}
			cout << tekst << endl << endl;
		}
		else
		{
			cout << "ERROR: Nie znaleziono pliku Splash.txt!";
		}
	}

	void PoziomTrudnosci()
	{
		cout << "Wybierz poziom trudnosci:\n";
		cout << "1. Latwy\n";
		cout << "2. Sredni\n";
		cout << "3. Trudny\n\n";
		int pt;
		cin >> pt;
		switch (pt)
		{
		case 1: { szybkosc = 1; break; }
		case 2: { szybkosc = 0.5; break; }
		case 3: { szybkosc = 0.1; break; }
		default:
			cout << "Nie zmieniono";
			break;
		}
		wait(1);
		system("cls");
	}

	void ZmienNick() 
	{
		cout << "Podaj nick" << endl;
		cin >> this->gracz;
		system("cls");
	}

	string ZwrocGracza()
	{
		return this->gracz;
	}

	void MainMenu()
	{
		char wybor, czyszczenie;
		bool poprawny_wybor;
		do
		{
			cout << "                       " << " Menu\n" << endl;
			cout << "                 " << " 1: Start\n";
			cout << "                 " << " 2: Tabela wynikow\n";
			cout << "                 " << " 3: Poziom trudnosci\n";
			cout << "                 " << " 4: Zmien nick\n";
			cout << "                 " << " 5: Wyjscie\n";

				poprawny_wybor = true;
				cin >> wybor;
				system("cls");

				switch (wybor)
				{
				case '1':
				{
					this->start = true;
					break;
				}
				case '2':
				{
					score.WyswietlWyniki();
					cout << "\n\nCzy chcesz wyczyscic wyniki? [Y/N]: \n";
					cin >> czyszczenie;
					if (czyszczenie == 'Y' || czyszczenie == 'y') score.CzyscWyniki();
					system("cls");
					break;
				}
				case '3':
				{
					PoziomTrudnosci();
					break;
				}
				case '4':
				{
					ZmienNick();
					break;
				}
				case '5':
				{
					graON = false;
					break;
				}
				default:
				{
					poprawny_wybor = false;
					cout << "Blad wybierania sprobuj ponownie \n\n";
					break;
				}
				}
			} while (poprawny_wybor == false);
				

	}
};


//wzi�te z neta z poprawkami
void wait(double seconds)
{
	clock_t endwait;
	endwait = clock() + seconds * CLOCKS_PER_SEC;
	while (clock() < endwait) {}
}


int main()
{

	char c;
	Menu menu;
	menu.SplashScreen();
	wait(5);

	Gra gra;
	system("cls");
	menu.ZmienNick();
	do 
	{
		menu.MainMenu();
		
		if (menu.start == true) 
		{
			gra.Reset();
			menu.start = false;
			while (gra.czy_brak_kolizji == true)
			{

				gra.Przesun();
				if (gra.ZwrocWynik()%3 == 0) gra.TworzPrzeszkode();

				if (_kbhit())
				{
					c = _getch();
					gra.PrzesunAutko(c);
				}
				cout << endl << "Wynik: " << gra.ZwrocWynik();
				wait(menu.szybkosc);
				gra.ZwiekszWynik();
				system("cls");
				gra.WyswietlTor();
				
				
			}
			system("cls");
			cout << "Koniec. Przegrales\n\nTwoj wynik to: ";
			cout << gra.ZwrocWynik();
			menu.score.DodajWynik(gra.ZwrocWynik(), menu.ZwrocGracza());
			
			wait(3);
			system("cls");
			menu.score.WyswietlWyniki();
			wait(3);
			system("cls");
		}		
	} while (menu.graON == true);
	cout << "Trwa zamykanie";
	wait(2);
	gra.~Gra();
	menu.score.~Wynik();
	menu.~Menu();
	
	return 0;
}